export const WEATHER_TYPES = {
  THUNDERSTORM: "thunderstorm",
  DRIZZLE: "drizzle",
  RAIN: "rain",
  SNOW: "snow",
  CLEAR: "clear",
  CLOUDS: "clouds",
};
